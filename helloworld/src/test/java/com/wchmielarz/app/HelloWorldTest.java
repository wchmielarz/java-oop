package com.wchmielarz.app;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by wchmielarz on 26.03.16.
 */
public class HelloWorldTest {

    String str = "Hello world";

    HelloWorld helloWorld = new HelloWorld();

    @Test
    public void helloWorldShouldPrintString(){
        assertEquals(str,helloWorld.message(str));
    }

}
